package com.firebase.proyecto_movil_naow.Fargmentos;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import com.firebase.proyecto_movil_naow.Modelo.Product;
import com.firebase.proyecto_movil_naow.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;


public class ProductosFragment extends Fragment {

    private ListView listView;
    private FirebaseListAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_productos, container, false);
        listView= (ListView)view.findViewById(R.id.listLista);

        DatabaseReference query = FirebaseDatabase.getInstance().getReference().child("Productos");
        FirebaseListOptions<Product> options = new FirebaseListOptions.Builder<Product>().
                setLayout(R.layout.layout_producto).
                setQuery(query, Product.class).build();

        adapter = new FirebaseListAdapter(options) {
            @Override
            protected void populateView(View v, Object model, int position) {
                ImageView imageView = (ImageView)v.findViewById(R.id.imagenProducto);
                TextView nombre = (TextView)v.findViewById(R.id.txtNombreProducto);
                TextView descripcion = (TextView)v.findViewById(R.id.txtDescripcionProducto);
                TextView precio = (TextView)v.findViewById(R.id.txtPrecioProducto);

                Product producto =(Product) model;
                nombre.setText(producto.getNombre().toString());
                descripcion.setText(producto.getDetalle().toString());
                precio.setText(producto.getValor().toString());

                String descarga = producto.getFoto();
                Picasso.with(getContext()).load(Uri.parse(descarga)).into(imageView);
            }

        };

        listView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}