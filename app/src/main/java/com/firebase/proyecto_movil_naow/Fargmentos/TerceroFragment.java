package com.firebase.proyecto_movil_naow.Fargmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.proyecto_movil_naow.Modelo.Usuario;
import com.firebase.proyecto_movil_naow.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TerceroFragment extends Fragment {

    private ListView listView;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener Auth;
    private DatabaseReference users;
    private String userId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_tercero, container, false);
        listView = (ListView)view.findViewById(R.id.ListUser);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        users = firebaseDatabase.getReference();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        userId = user.getUid();


        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Datos(dataSnapshot);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        return view;
    }

    public void Datos(DataSnapshot dataSnapshot){
        for(DataSnapshot ds : dataSnapshot.getChildren()){
                Usuario usuario = ds.getValue(Usuario.class);

             Toast.makeText(getContext(), "Nombre: "+usuario.getNombres(), Toast.LENGTH_LONG).show();
//            usuariooo.setNombres(getString(R.string.nombre)+ " "+ds.child(userId).getValue(Usuario.class).getNombres());
//            usuariooo.setApellidos(getString(R.string.apellido)+" "+ds.child(userId).getValue(Usuario.class).getApellidos());
//            usuariooo.setCedula(getString(R.string.CEDULA)+" "+ds.child(userId).getValue(Usuario.class).getCedula());
//            usuariooo.setCelular(getString(R.string.CELL)+" "+ds.child(userId).getValue(Usuario.class).getCelular());
//            usuariooo.setUbicacion(getString(R.string.UBICACION)+" "+ ds.child(userId).getValue(Usuario.class).getUbicacion());
//            usuariooo.setCorreo(getString(R.string.CORREO)+" "+ds.child(userId).getValue(Usuario.class).getCorreo());

           // ArrayList<String> array  = new ArrayList<>();
           // array.add(usuariooo.getApellidos());
           // array.add(usuariooo.getNombres());
//            array.add(usuariooo.getCedula());
//            array.add(usuariooo.getCelular());
//            array.add(usuariooo.getUbicacion());
//            array.add(usuariooo.getCorreo());
           // ArrayAdapter adapter = new ArrayAdapter(getContext() ,android.R.layout.simple_list_item_1,array);
           // listView.setAdapter(adapter);
        }

    }
}
