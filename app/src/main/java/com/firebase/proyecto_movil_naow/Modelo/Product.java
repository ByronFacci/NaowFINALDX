package com.firebase.proyecto_movil_naow.Modelo;

public class Product {
    private String Detalle, Foto, Nombre, Valor;

    public Product(){
        this.Detalle = Detalle;
        this.Foto = Foto;
        this.Nombre = Nombre;
        this.Valor = Valor;
    }

    public String getDetalle() {
        return Detalle;
    }

    public void setDetalle(String detalle) {
        Detalle = detalle;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }
}
