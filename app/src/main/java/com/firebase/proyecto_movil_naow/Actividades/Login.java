package com.firebase.proyecto_movil_naow.Actividades;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.firebase.proyecto_movil_naow.Modelo.Usuario;
import com.firebase.proyecto_movil_naow.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

public class Login extends AppCompatActivity implements View.OnClickListener{

    private EditText usuario, contraseña;
    private Button iniciar, registrarse;
    private ProgressDialog progressDialog;
    private ViewFlipper viewFlipper;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usuario = (EditText)findViewById(R.id.TXTUser);
        contraseña = (EditText)findViewById(R.id.TXTPass);
        iniciar = (Button)findViewById(R.id.BTNIniciarSesion);
        registrarse = (Button)findViewById(R.id.BTNRegistro);
        iniciar.setOnClickListener(this);
        registrarse.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        viewFlipper = (ViewFlipper)findViewById(R.id.ViewFliper);
        int imagenes[] = {R.drawable.energy, R.drawable.alcalina, R.drawable.logo, R.drawable.gas, R.drawable.smart};
        for (int i = 0; i< imagenes.length; i++){
        fliper(imagenes[i]);
        }

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    startActivity(new Intent(Login.this, Productos.class));
                    finish();
                } else {

                }
            }
        };
    }


    public void fliper(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.BTNIniciarSesion:
                if (usuario.getText().toString().isEmpty()) {
                    usuario.setError(getResources().getString(R.string.error));
                } else if (contraseña.getText().toString().isEmpty()) {
                    contraseña.setError(getResources().getString(R.string.error));
                } else {
                    if (usuario.getText().toString().isEmpty()) {
                        usuario.setError(getResources().getString(R.string.error));
                    } else if (contraseña.getText().toString().isEmpty()) {
                        contraseña.setError(getResources().getString(R.string.error));
                    } else {
                        final String correo = usuario.getText().toString().trim();
                        String clave = contraseña.getText().toString().trim();
                        progressDialog.setTitle("CONSULTANDO EN LÍNEA");
                        progressDialog.setMessage("ACCEDIENDO..!");
                        progressDialog.show();
                        firebaseAuth.signInWithEmailAndPassword(correo, clave).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                Intent intent = new Intent(Login.this, Productos.class);
                                startActivity(intent);
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(Login.this, "CREDENCIALES ERRONEAS..!", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                            }
                        });
                    }
                }
                break;

            case R.id.BTNRegistro:
                startActivity(new Intent(Login.this, UsuarioNuevo.class));
                finish();
                break;

            }
        }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            firebaseAuth.removeAuthStateListener(mAuthListener);
        }
    }
}
